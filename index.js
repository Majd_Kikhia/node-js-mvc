const express = require('express')
const mydb = require('./config/database')
const router = require('./routes/router')
const app = express()


app.use(router)

app.listen(6060,()=>{
    console.log('server')
})